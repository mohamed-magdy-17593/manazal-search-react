import http from '../http'

export const getCountries = () =>
  http.get(`/countries`).then(res => res.countries)

export const getCities = countryId =>
  http.get(`/cities_country/${countryId}`).then(res => res.cities)

export const getPropTypes = () =>
  http.get(`/prop_types/en`).then(res => res.types)
