import http from '../http'

export const search = (searchValues = {}) =>
  http.post('/search_wot', searchValues)
