import axios from 'axios'
import {showError} from '../components/ErrorHandler'

export const API_URL = 'http://manazal.rabbittec.com/api'

export const http = axios.create({
  baseURL: API_URL,
})

const runOnce = (function() {
  let flag = 0
  return fn => {
    if (flag) {
      return
    } else {
      flag = 1
      return fn()
    }
  }
})()

http.interceptors.response.use(
  res => res.data,
  err => {
    const expectedError =
      err.response && err.response.status >= 400 && err.response.status < 500
    if (!expectedError) {
      // handle http unexepted errors case
      runOnce(() => {
        showError(err)
      })
    }
    return Promise.reject(err)
  },
)

export default http
