import React, {Component} from 'react'
import Loading from '../ui/Loading/Loading'
import {withSearchConsumer} from './contexts/SearchContext'
import {search} from '../http/api/search'
import compose from 'ramda/src/compose'
import {withUniqKey} from '../utils/withUniqKey'
import {PropsList} from './PropsList'
import {AppButton} from '../ui/Inputs/AppButton'

const totalPages = (count, perPage) => Math.ceil(count / perPage)

export const SearchContent = compose(
  withSearchConsumer,
  withUniqKey('searchValues'),
)(
  class extends Component {
    state = {
      loading: true,
      nextPage: 1,
      searchData: [],
    }
    _isMounted = true
    fetchSearch = () => {
      const {nextPage: page} = this.state
      this._isMounted && this.setState({loading: true})
      search({...this.props.searchValues, page}).then(
        res =>
          this._isMounted &&
          this.setState(({searchData}) => ({
            nextPage: totalPages(res.count, res.perPage) > page ? page + 1 : -1,
            searchData: [...searchData, ...res.props],
            loading: false,
          })),
      )
    }
    componentDidMount() {
      setTimeout(() => {
        this._isMounted && this.fetchSearch()
      }, 500)
    }
    componentWillUnmount() {
      this._isMounted = false
    }
    render() {
      const {nextPage, searchData, loading} = this.state
      return (
        <>
          <PropsList props={searchData} loading={loading} />
          {loading ? (
            <Loading />
          ) : (
            nextPage !== -1 && (
              <AppButton onClick={this.fetchSearch} type="button" block>
                Load more
              </AppButton>
            )
          )}
        </>
      )
    }
  },
)
