import React, {Component} from 'react'
import RenderPropToHOC from '../../utils/RenderPropToHOC'

const {Consumer, Provider} = React.createContext()

const formatSearchValues = searchValues =>
  Object.entries(searchValues).reduce(
    (acc, [key, value]) => ({
      ...acc,
      ...(value ? {[key]: value} : {}),
    }),
    {},
  )

class SearchProvider extends Component {
  initialSearchValues = {
    lang: 'en',
    key: '',
    is_rent: '',
    is_commercial: '',
    max_price: '',
    min_price: '',
    country_id: '',
    city_id: '',
    prop_type_id: '',
  }
  setSearchValues = searchValues =>
    this.setState({searchValues: formatSearchValues(searchValues)})
  state = {
    searchValues: formatSearchValues(this.initialSearchValues),
    initialSearchValues: this.initialSearchValues,
    setSearchValues: this.setSearchValues,
  }
  render() {
    return <Provider value={this.state}>{this.props.children}</Provider>
  }
}

const withSearchConsumer = RenderPropToHOC(Consumer)

export {SearchProvider, Consumer as SearchConsumer, withSearchConsumer}
