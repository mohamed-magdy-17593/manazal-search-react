import React from 'react'
import {css} from 'react-emotion'
import PropImage from './PropsImages/PropImage'
import {slow, medium} from '../ui/times'
import {getImagesOfProp} from './getImagesOfProp'

export const PropCard = ({prop}) =>
  console.log(prop) || (
    <div
      className={css`
        position: relative;
        display: flex;
        align-items: center;
        overflow: hidden;
        width: 100%;
        height: 200px;
        margin-bottom: 20px;
        border-radius: 0.25rem;
        &:hover > img {
          filter: blur(2px) grayscale(0%);
        }
        &:hover > div > .address {
          transform: translate(-50%, -300%);
        }
        &:hover > div > .price {
          transform: translate(-50%, -50%);
        }
      `}
    >
      <div
        className={css`
          overflow: hidden;
          padding: 10px;
          background: rgba(49, 39, 54, 0.6);
          text-align: center;
          width: 100%;
          height: 80px;
          position: absolute;
          font-family: 'Slabo 27px', serif;
          color: white;
          z-index: 9;
          font-size: 2rem;
          font-weight: lighter;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        `}
      >
        <span
          className={`${css`
            transition: transform ${medium}ms ease-in;
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
          `} address`}
        >{`${prop.country} / ${prop.city}`}</span>
        <span
          className={`${css`
            transition: transform ${medium}ms ease-in;
            display: block;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -300%);
          `} price`}
        >{`${prop.price} ${prop.currency_code}`}</span>
      </div>
      <PropImage
        src={getImagesOfProp(prop)[0]}
        className={css`
          width: 100%;
          min-height: 100%;
          filter: blur(0px) grayscale(80%);
          transition: filter ${slow}ms ease-out;
        `}
      />
    </div>
  )
