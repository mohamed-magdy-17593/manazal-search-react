import React from 'react'
import {PropCard} from './PropCard'

export const PropsList = ({props, loading}) =>
  !loading && props.length === 0 ? (
    <div className="text-center">
      <h2>Sorry there is no properties</h2>
      <a href="#">Request What You looking for</a>
    </div>
  ) : (
    props.map(prop => <PropCard key={prop.id} prop={prop} />)
  )
