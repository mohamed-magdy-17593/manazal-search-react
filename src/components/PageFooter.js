import React from 'react'
import Footer from '../ui/Footer/Footer'
import {css} from 'emotion'

export const PageFooter = () => (
  <Footer>
    <p
      className={css`
        margin: 0;
        text-align: center;
        color: white;
      `}
    >
      Copyright @ {new Date().getFullYear()} Built By Mohamed Magdy. All Rights
      Reserved
    </p>
  </Footer>
)

export default PageFooter
