import React from 'react'
import Img from 'react-image'
import {API_URL} from '../../http/http'
import F from './fallback.png'

const PropImage = ({src, ...props}) => (
  <Img src={[src && `${API_URL}/${src}`, F]} {...props} />
)

export default PropImage
