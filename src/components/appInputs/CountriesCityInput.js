import React from 'react'
import {AppInput} from '../../ui/Inputs/AppInput'
import {CountriesProvider} from '../apiProviders/CountriesProvider'
import {CityProvider} from '../apiProviders/cityProvider'

export const CountriesCityInput = ({values, onChange, setFieldValue}) => (
  <>
    <CountriesProvider>
      {({countries}) => (
        <AppInput
          type="select"
          name="country_id"
          value={values.country_id}
          onChange={e => {
            setFieldValue('city_id', '')
            onChange(e)
          }}
          disabled={countries.length === 0}
        >
          <option value="">-countries-</option>
          {countries.map(country => (
            <option key={country.id} value={country.id}>
              {country.name}
            </option>
          ))}
        </AppInput>
      )}
    </CountriesProvider>
    <CityProvider countryId={values.country_id}>
      {({cities}) => (
        <AppInput
          type="select"
          name="city_id"
          value={values.city_id}
          onChange={onChange}
          disabled={cities.length === 0}
        >
          <option value="">-cities-</option>
          {cities.map(city => (
            <option key={city.id} value={city.id}>
              {city.name}
            </option>
          ))}
        </AppInput>
      )}
    </CityProvider>
  </>
)
