import React from 'react'
import {AppInput} from '../../ui/Inputs/AppInput'

export const MinMaxPriceInput = ({values, onChange}) => (
  <>
    <AppInput
      type="number"
      name="min_price"
      value={values.min_price}
      onChange={onChange}
      placeholder="Min Price"
    />
    <AppInput
      type="number"
      name="max_price"
      value={values.max_price}
      onChange={onChange}
      placeholder="Max Price"
    />
  </>
)
