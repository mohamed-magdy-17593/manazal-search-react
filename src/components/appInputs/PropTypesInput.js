import React from 'react'
import {AppInput} from '../../ui/Inputs/AppInput'
import {PropTypeProvider} from '../apiProviders/PropTypeProvider'

export const PropTypeInput = ({values, onChange}) => (
  <PropTypeProvider>
    {({propTypes}) => (
      <AppInput
        type="select"
        name="prop_type_id"
        value={values.prop_type_id}
        onChange={onChange}
        disabled={propTypes.length === 0}
      >
        <option value="">-Property type-</option>
        {propTypes.map(proptype => (
          <option key={proptype.id} value={proptype.id}>
            {proptype.name}
          </option>
        ))}
      </AppInput>
    )}
  </PropTypeProvider>
)
