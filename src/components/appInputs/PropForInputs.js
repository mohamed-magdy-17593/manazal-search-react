import React from 'react'
import {AppInput} from '../../ui/Inputs/AppInput'

export const PropForInputs = ({values, onChange}) => (
  <>
    <AppInput
      type="select"
      name="is_rent"
      value={values.is_rent}
      onChange={onChange}
    >
      <option value="">-all-</option>
      <option value="0">Sale</option>
      <option value="1">Rent</option>
    </AppInput>
    <AppInput
      type="select"
      name="is_commercial"
      value={values.is_commercial}
      onChange={onChange}
    >
      <option value="">-all-</option>
      <option value="0">Residential</option>
      <option value="1">Commercial</option>
    </AppInput>
  </>
)
