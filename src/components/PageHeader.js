import React from 'react'
import Header from '../ui/Header/Header'
import HeaderText from '../ui/Header/HeaderText'
import HeaderSlice from '../ui/Header/HeaderSlice'
import Bounce from 'react-reveal/Bounce'

export const PageHeader = () => (
  <Header>
    <Bounce>
      <HeaderText>
        <HeaderSlice>Manazal</HeaderSlice>
      </HeaderText>
    </Bounce>
  </Header>
)

export default PageHeader
