import {Component} from 'react'
import {getPropTypes} from '../../http/api/countryCity'

export class PropTypeProvider extends Component {
  state = {
    propTypes: [],
  }
  componentDidMount() {
    getPropTypes().then(propTypes => this.setState({propTypes}))
  }
  render() {
    return this.props.children({propTypes: this.state.propTypes})
  }
}
