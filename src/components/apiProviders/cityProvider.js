import {Component} from 'react'
import {getCities} from '../../http/api/countryCity'
import {withUniqKey} from '../../utils/withUniqKey'

export const CityProvider = withUniqKey('countryId')(
  class AppCityProvider extends Component {
    state = {
      cities: [],
    }
    componentDidMount() {
      const {countryId} = this.props
      this.setState({cities: []})
      if (countryId) {
        getCities(countryId).then(cities => this.setState({cities}))
      }
    }
    render() {
      return this.props.children({cities: this.state.cities})
    }
  },
)
