import {Component} from 'react'
import {getCountries} from '../../http/api/countryCity'

export class CountriesProvider extends Component {
  state = {
    countries: [],
  }
  componentDidMount() {
    getCountries().then(countries => this.setState({countries}))
  }
  render() {
    return this.props.children({countries: this.state.countries})
  }
}
