import React from 'react'
import Main from '../ui/Main/Main'
import {Container, Row, Col} from 'reactstrap'
import {SearchContent} from './SearchContent'
import {SearchSideBar} from './SearchSideBar'
import {SearchProvider} from './contexts/SearchContext'

export const PageMain = () => (
  <Main>
    <SearchProvider>
      <Container>
        <Row>
          <Col xs="12" md="4">
            <SearchSideBar />
          </Col>
          <Col xs="12" md="8">
            <SearchContent />
          </Col>
        </Row>
      </Container>
    </SearchProvider>
  </Main>
)

export default PageMain
