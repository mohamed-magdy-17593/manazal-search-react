import React, {Component} from 'react'
import EventEmiter from 'events'
import {css} from 'react-emotion'

const errEvent = new EventEmiter()

const ErrorView = ({err}) =>
  console.log(err) || (
    <div
      className={css`
        z-index: 9999;
        background-color: white;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        display: flex;
        justify-content: center;
        align-items: center;
      `}
    >
      <div>
        <h1
          className={css`
            font-size: 5rem;
          `}
        >
          {err && err.response && err.response.status}
        </h1>
        <h1>Unexpected Error</h1>
      </div>
    </div>
  )

export class ErrorHandler extends Component {
  state = {err: null}
  componentDidCatch(err) {
    this.setState({err})
  }
  componentDidMount() {
    errEvent.on('err', err => this.setState({err}))
  }
  render() {
    return (
      <>
        {this.state.err && <ErrorView err={this.state.err} />}
        {this.props.children}
      </>
    )
  }
}

export const showError = err => errEvent.emit('err', err)
