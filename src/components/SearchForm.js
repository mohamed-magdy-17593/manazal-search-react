import React from 'react'
import {Form} from 'reactstrap'
import {dark} from '../ui/colors'
import {Formik} from 'formik'
import {Hr} from '../ui/Hr'
import {AppInput} from '../ui/Inputs/AppInput'
import {AppButton} from '../ui/Inputs/AppButton'
import {PropForInputs} from './appInputs/PropForInputs'
import {MinMaxPriceInput} from './appInputs/MinMaxPriceInput'
import {CountriesCityInput} from './appInputs/CountriesCityInput'
import {PropTypeInput} from './appInputs/PropTypesInput'
import {SearchConsumer} from './contexts/SearchContext'
import {DoActionOnChange} from '../utils/DoActionOnChange'

export const SearchForm = () => (
  <SearchConsumer>
    {({initialSearchValues, setSearchValues}) => (
      <Formik
        initialValues={{
          ...initialSearchValues,
        }}
      >
        {({values, handleChange, handleReset, setFieldValue}) => (
          <Form>
            <DoActionOnChange
              value={values}
              action={() => setSearchValues(values)}
            />
            <Hr />
            <AppInput
              name="key"
              value={values.key}
              onChange={handleChange}
              borderColor={dark}
              placeholder="What you need"
            />
            <Hr />
            <h6>Property For:</h6>
            <PropForInputs values={values} onChange={handleChange} />
            <Hr />
            <h6>Address:</h6>
            <CountriesCityInput
              values={values}
              onChange={handleChange}
              setFieldValue={setFieldValue}
            />
            <Hr />
            <h6>Property Price:</h6>
            <MinMaxPriceInput values={values} onChange={handleChange} />
            <Hr />
            <h6>Property Type:</h6>
            <PropTypeInput values={values} onChange={handleChange} />
            <Hr />
            <AppButton type="button" onClick={handleReset} block>
              Reset
            </AppButton>
          </Form>
        )}
      </Formik>
    )}
  </SearchConsumer>
)
