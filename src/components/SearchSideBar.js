import React from 'react'
import {CustomCard} from '../ui/Card/CustomCard'
import {css} from 'emotion'
import {SearchForm} from './SearchForm'

export const SearchSideBar = () => (
  <CustomCard>
    <h4
      className={css`
        font-weight: bold;
      `}
    >
      Search for:
    </h4>
    <SearchForm />
  </CustomCard>
)
