export const getImagesOfProp = prop =>
  prop.medias
    .map(({type, src}) => (type === 'photo' || type === 'plan') && src)
    .filter(i => i)
