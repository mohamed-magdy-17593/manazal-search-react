import React, {Component} from 'react'
import {PageHeader} from './components/PageHeader'
import {PageFooter} from './components/PageFooter'
import {PageMain} from './components/PageMain'
import {ErrorHandler} from './components/ErrorHandler'

const App = () => (
  <ErrorHandler>
    <PageHeader />
    <PageMain />
    <PageFooter />
  </ErrorHandler>
)

export default App
