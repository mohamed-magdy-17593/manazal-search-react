import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import 'bootstrap/dist/css/bootstrap.min.css'
import './ui/global'

ReactDOM.render(<App />, document.getElementById('root'))
