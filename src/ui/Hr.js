import {gray} from './colors'
import styled from 'react-emotion'

export const Hr = styled('hr')`
  border-color: ${gray};
`
