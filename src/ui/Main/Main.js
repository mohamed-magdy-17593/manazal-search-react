import styled from 'react-emotion'

const Main = styled('main')`
  padding: 40px 10px;
  min-height: 600px;
`

export default Main
