import React from 'react'
import {css} from 'react-emotion'
import {Card, CardBody} from 'reactstrap'
import {gray} from '../colors'

export const CustomCard = ({children}) => (
  <Card
    className={css`
      /* border: 2px solid ${gray}; */
      border: none;
      background-color: #f5f6fa
    `}
  >
    <CardBody>{children}</CardBody>
  </Card>
)
