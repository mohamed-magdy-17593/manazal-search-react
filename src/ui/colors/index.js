export const dark = '#312736'

export const gray = '#D9D9D9'

export const red = '#D4838F'

export const redesh = '#D6ABB1'

export const green = '#C4FFEB'

export const lightColor = '#f5f6fa'
