import {injectGlobal} from 'emotion'
import {dark, gray} from '../colors'

injectGlobal`
  @import url('https://fonts.googleapis.com/css?family=Slabo+27px');
  @import url('https://fonts.googleapis.com/css?family=K2D');
  body {
    font-family: 'K2D', sans-serif;
    color: ${dark}
  }
  ::placeholder {
    color:${gray};
  }
`
