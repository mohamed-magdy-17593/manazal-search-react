import styled from 'react-emotion'
import {dark} from '../colors'

const Footer = styled('footer')`
  background-color: ${dark};
  padding: 40px 10px;
`

export default Footer
