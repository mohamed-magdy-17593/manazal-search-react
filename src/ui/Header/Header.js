import styled from 'react-emotion'
import {green} from '../colors'
import {slow} from '../times'

const Header = styled('header')`
  cursor: crosshair;
  transition: padding ${slow}ms ease-out;
  padding: 30px 10px;
  background-color: ${green};
  &:hover {
    padding: 50px 10px;
  }
`

export default Header
