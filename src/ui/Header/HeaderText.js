import styled from 'react-emotion'
import {dark} from '../colors'

const HeaderText = styled('h1')`
  margin: 0;
  font-family: 'Slabo 27px', serif;
  color: ${dark};
  text-align: center;
  font-size: 9rem;
  font-weight: bold;
`

export default HeaderText
