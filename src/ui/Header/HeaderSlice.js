import React from 'react'
import styled from 'react-emotion'
import {red} from '../colors'
import {slow} from '../times'

const Span = styled('span')`
  transition: all ${slow}ms ease-out;
  display: inline-block;
  text-shadow: 0px 5px 0 white, 0 0 0 ${red};
  &:hover {
    transform: scale(1.2);
    text-shadow: 0px -5px 0 ${red};
  }
`

const HeaderSlice = ({children = ''}) =>
  [...children].map((c, i) => <Span key={i}>{c}</Span>)

export default HeaderSlice
