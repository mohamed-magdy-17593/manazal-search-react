import React from 'react'
import {FormGroup, Input} from 'reactstrap'
import {css} from 'emotion'
import {gray} from '../colors'

export const AppInput = ({type, className, borderColor = gray, ...props}) => (
  <FormGroup>
    <Input
      className={`${css`
        border: 2px solid ${borderColor};
      `} ${className}`}
      type={type || 'text'}
      {...props}
    />
  </FormGroup>
)
