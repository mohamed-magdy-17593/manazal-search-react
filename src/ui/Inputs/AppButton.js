import React from 'react'
import {Button} from 'reactstrap'
import {css} from 'emotion'
import {green, dark} from '../colors'

export const AppButton = ({
  type,
  className,
  buttonStyle = 'dark',
  ...props
}) => (
  <Button
    type={type || 'submit'}
    className={css`
      color: ${dark};
      border: 2px solid ${green};
      background-color: ${buttonStyle === 'dark' ? green : 'white'};
      &:hover,
      &:focus,
      &:active,
      &:active:focus {
        color: ${dark} !important;
        border: 2px solid ${dark} !important;
        background-color: white !important;
      }
    `}
    {...props}
  />
)
