import React from 'react'

const RenderPropToHOC = RenderProp => WrapComponent => props => (
  <RenderProp>{value => <WrapComponent {...value} {...props} />}</RenderProp>
)

export default RenderPropToHOC
