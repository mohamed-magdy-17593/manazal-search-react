import {Component} from 'react'
import {withUniqKey} from './withUniqKey'

export const DoActionOnChange = withUniqKey('value')(
  class extends Component {
    componentDidMount() {
      this.props.action()
    }
    render() {
      return null
    }
  },
)
