import React from 'react'
import {J} from './J'

export const withUniqKey = propName => WrapComponent => props => (
  <WrapComponent key={J(props[propName])} {...props} />
)
